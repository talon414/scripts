#!/bin/python
import math
import microfeedline as msf
import impedencetransform as itf

c0=3e8

def wpatch(er,frq):

    return c0*math.sqrt(2/(er+1))/(2*frq)


def ereff(er,W,h):

    if W/h > 1:

        return (er+1)/2+(er-1)*math.pow(1+12*h/W,-1/2)/2

    raise f"condition W/h > 1 failed: W/h={W/h}"


def lapprox(e_reff,W,h):

    return 0.412*(e_reff+0.3)*(W/h+0.264)*h/((e_reff-0.258)*(W/h+0.8))


def lpatch(er,frq,e_reff,W,h):
    #print(e_reff)
    return (c0/(2*frq*math.sqrt(e_reff))) - 2*lapprox(e_reff,W,h)



def inpZ0(W,frq,h):

    ld=c0/frq

    if h/ld < 1/10:
        G=W*(1-math.pow(2*math.pi*h/ld,2))/(120*ld)


    #print(G,1/(2*G))

    return 1/(2*G)

def recpatchd(er,frq,h):

    frq=frq*1e9
    sfZ0=50
    cvf=1000
    h=h/cvf
    res=[]

    wp=wpatch(er,frq)

    lp=lpatch(er,frq,ereff(er,wp,h),wp,h)
    edgZ0=inpZ0(wp,frq,h)

    mwid=msf.mswid(msf.Wd_ratio(er,sfZ0),h)

    #matching

    mtres=itf.dquatwtr(edgZ0,er,frq,h)

    return f"Width(mm): {wp*cvf}\nLength(mm): {lp*cvf}\nW/L ratio: {wp/lp}\nInput Impedence(Edge): {edgZ0}\nFeedline width(mm): {mwid*cvf}\nImpedence Matching\n{mtres}"



if __name__=='__main__':
    er=2.2
    h=1.575 #mm
    fq=2.1 # GHz


    #print(wpatch(er,fq))
    print(recpatchd(er,fq,h))
    #print(lapprox(er,wpatch(er,fq),h))
