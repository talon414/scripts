#!/bin/python
import math


# globals

global c0
c0=3e8



def const_B(er,Z0) -> int:

    return 377*math.pi/(2*Z0*pow(er,1.0/2))

def const_A(er,Z0) -> int:

    return Z0*pow((er+1)/2,1.0/2)/60+(er-1)*(0.23+0.11/er)/(er+1)



def Wd_ratio(er,Z0):

    _A=const_A(er,Z0)

    # W/d < 2
    wd1=8.0*math.exp(_A)/(math.exp(2*_A)-2)

    if wd1<2:

        return wd1

    else:
        _B=const_B(er,Z0)

        # W/d > 2
        wd2=2.*(_B-1-math.log(2*_B-1)+(er-1)*(math.log(_B-1)+0.39-0.61/er)/(2*er))/math.pi

        return wd2


def e_eff(er,wdrat):
    # effective dielectric constant

    return (er+1)/2+(er-1)*(1/math.sqrt(1+12/wdrat))/2


def char_imp(eef,wdrat):
    # Compute characteristic impedence if e effective and W/d ratio are given

    if wdrat <= 1:

        return 60*math.log(8/wdrat+wdrat/4)

    else:

        return 120*math.pi/(math.sqrt(eef)*(wdrat+1.393+0.667*math.log(wdrat+1.444)))


def alp_d(f0,eef,er,delta):

    # attenuation due to dielectric loss Np/m.

    k0=2*math.pi/(c0/f0)

    return k0*er*(eef-1)*math.tan(delta)/(2*math.sqrt(eef)*(er-1))

def alp_c(omg,u0,sig,Z0,W):
    # attenuation due to conductor loss Np/m.

    return math.sqrt(omg*u0/(2*sig))/(Z0*W)

def mslen(phase_delay,frq,eef):

    # phase delay in deg

    k0=2*math.pi/(c0/frq) # m^-1.

    return phase_delay*(math.pi/180)/(math.sqrt(eef)*k0) # m.

def mswid(wdrat,d):

    return wdrat*d



#################################################################

if __name__=='__main__':
    pd=90
    er=2.2
    f=2.1e9
    r1=50/(2**0.5)
    #r1=101.40446
    r1=50
    h=1.575


    klen=mslen(pd,f,e_eff(er,Wd_ratio(er,r1)))*1000
    print(klen)
    print(mswid(Wd_ratio(er,r1),h))
    #
    #
    #
    #
    # print(Wd_ratio(er,r1))
